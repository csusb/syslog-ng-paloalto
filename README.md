syslog-ng parsers for Palo Alto PAN-OS 7.0 Syslogs
==================================================

Column names and csv-parser flags derived from:
https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/monitoring/syslog-field-descriptions


Intended to be included early on with in the `conf.d` with something like:
```
#/etc/syslog-ng/conf.d/00-more-includes.conf

@include "/etc/syslog-ng/conf.d/paloalto/*.conf"
```


Example usage
=============

See '20-logging.example'

